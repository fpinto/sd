package sd;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

final class Client {

	private static final Logger LOGGER = Logger.getLogger( Client.class.getName() );

	private static String nome;

	private static boolean first = true;

	private static Game game;

	private Client() {

		LOGGER.setLevel(Level.INFO);

	}

	public static void main(String[] args) {

		try {

			game = new Game();

			Socket sock = new Socket("localhost", 8082);

			start(sock);

		} catch (IOException e) {

			System.out.println(e.getMessage());

		}
	}

	private static void start(Socket socket) {

		try {

			if (socket.isConnected()) {

				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());

				BufferedReader bRInput;
				bRInput = new BufferedReader(new InputStreamReader(System.in));

				while (true) {

					boolean receive = false;

					if (first) {

						System.out.print("Username: ");

						nome = bRInput.readLine();

						byte[] msg = encodeClient2Server(nome);

						out.write(msg);

						LOGGER.log(Level.FINE, msg.toString());

						out.flush();

						receive = true;

						first = false;

					} else {

						String fromUser = bRInput.readLine();

						byte[] msg = decodeUserInput(fromUser, nome);

						if ("9".equals(fromUser)) {

							ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

							outputStream.write((byte) -9);
							outputStream.write((byte) -9);

							byte[] exitMsg = outputStream.toByteArray();

							out.write(exitMsg);

							LOGGER.log(Level.FINE, exitMsg.toString());

							out.flush();

							System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nReceived exit message.");

							break;

						} else if ("1".equals(fromUser) || "2".equals(fromUser) || "0".equals(fromUser)) {

							out.write(msg);

							LOGGER.log(Level.FINE, msg.toString());

							out.flush();

							receive = true;

						}

					}

					if (receive) {

						byte[] buffer = new byte[50];

						ByteArrayOutputStream baos = new ByteArrayOutputStream();

						in.read(buffer);

						baos.write(buffer);

						while (0 < in.available()) {

							in.read(buffer);

							baos.write(buffer);

						}

						byte[] msg = baos.toByteArray();

						decodeServer2Client(msg);

						LOGGER.log(Level.FINE, msg.toString());

						printMenu();

						game.printGameResults();

					}

				}

				socket.close();

			}

		} catch (Exception e) {

			LOGGER.log( Level.SEVERE, e.toString(), e );

		}
	}

	private static byte[] encodeClient2Server(String name, int i) throws UnsupportedEncodingException {


		byte[] binNomeUser = name.getBytes("UTF-8");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		outputStream.write((byte) game.getScoreTeamOne());
		outputStream.write((byte) game.getScoreTeamTwo());

		outputStream.write((byte) binNomeUser.length);
		for (byte value : binNomeUser) {

			outputStream.write(value);

		}

		outputStream.write((byte) i);
		outputStream.write((byte) (i >>> 8));

		return outputStream.toByteArray();

	}

	private static byte[] encodeClient2Server(String name) throws UnsupportedEncodingException {


		byte[] binNomeUser = name.getBytes("UTF-8");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		outputStream.write((byte) game.getScoreTeamOne());
		outputStream.write((byte) game.getScoreTeamTwo());

		outputStream.write((byte) binNomeUser.length);
		for (byte value : binNomeUser) {

			outputStream.write(value);

		}

		if (first) {

			outputStream.write((byte) 0);
			outputStream.write((byte) 0);

		} else {

			outputStream.write((byte) game.getNumberOfUpdates());
			outputStream.write((byte) (game.getNumberOfUpdates() >>> 8));

		}

		return outputStream.toByteArray();

	}


	private static void decodeServer2Client(byte[] msg) throws UnsupportedEncodingException {

		byte binLenghtNameOne = msg[0];

		byte[] binNameOne = new byte[(int) binLenghtNameOne];

		System.arraycopy(msg, 1, binNameOne, 0, (int) binLenghtNameOne);

		byte binLenghtNameTwo = msg[(int) binLenghtNameOne + 1];


		byte[] binNameTwo = new byte[(int) binLenghtNameTwo];

		for (int i = 0; i < (int) binLenghtNameTwo; i++) {

			binNameTwo[i] = msg[i + (int) binLenghtNameOne + 2];

		}

		game.setScoreTeamOne((int) msg[(int) binLenghtNameOne + (int) binLenghtNameTwo + 2]);
		game.setScoreTeamTwo((int) msg[(int) binLenghtNameOne + (int) binLenghtNameTwo + 3]);

		byte[] count = new byte[2];
		count[0] = msg[(int) binLenghtNameOne + (int) binLenghtNameTwo + 4];
		count[1] = msg[(int) binLenghtNameOne + (int) binLenghtNameTwo + 5];

		game.setNumberOfUpdates(Utils.byteArray2Int(count));

		/*for (byte b : count) { //echo count as binary
			System.out.println(Integer.toBinaryString(b & 255 | 256).substring(1));
		}*/

		game.setNameTeamOne(new String(binNameOne, "UTF-8"));
		game.setNameTeamTwo(new String(binNameTwo, "UTF-8"));

	}

	private static byte[] decodeUserInput( String input, String nome) throws UnsupportedEncodingException {

		switch (input) {
			case "1":

				game.scoreOnePlus();
				break;

			case "2":

				game.scoreTwoPlus();
				break;

			case "0":

				return encodeClient2Server(nome,0);

			default:

				return new byte[1];

		}

		return encodeClient2Server(nome);

	}

	private static void printMenu(){
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n###############--Menu--###############");
		System.out.println("1 - Goal for " + game.getNameTeamOne() + '\n' +
				"2 - Goal for " + game.getNameTeamTwo() + '\n' +
				"0 - Get updated result\n" +
				"...\n" +
				"9 - Exit Program\n" +
				'\n');
	}

}
