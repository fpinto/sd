package sd;

import java.util.ArrayList;

class Game {

	private String nameTeamOne;
	private String nameTeamTwo;
	private int scoreTeamOne;
	private int scoreTeamTwo;
	private int numberOfUpdates;
	private boolean first = true;
	static ArrayList<Comment> comments = new ArrayList();

	public Game(String nameTeamOne, String nameTeamTwo) {

		this.nameTeamOne = nameTeamOne;
		this.nameTeamTwo = nameTeamTwo;
		scoreTeamOne = 0;
		scoreTeamTwo = 0;
		numberOfUpdates = 1;

	}

	public Game() {

		scoreTeamOne = 0;
		scoreTeamTwo = 0;
		numberOfUpdates = 1;

	}

	public final void printGameResults() {

		int tmp_updates = numberOfUpdates - 1;

		System.out.println("---------------------///////////////---------------------");

		System.out.println(nameTeamOne + ":  Score: " + scoreTeamOne);

		System.out.println(nameTeamTwo + ":  Score: " + scoreTeamTwo);

		System.out.println("---------------------///////////////---------------------");

		System.out.println("Number of score updates: " + tmp_updates + "\n\n");

	}

	public final String getNameTeamOne() {
		return nameTeamOne;
	}

	public final String getNameTeamTwo() {
		return nameTeamTwo;
	}

	public final int getScoreTeamOne() {
		return scoreTeamOne;
	}

	public final int getScoreTeamTwo() {
		return scoreTeamTwo;
	}

	public final int getNumberOfUpdates() {
		return numberOfUpdates;
	}

	public final synchronized ArrayList<Comment> getComments() {
		return comments;
	}

	public synchronized final void setNameTeamOne(String nameTeamOne) {
		this.nameTeamOne = nameTeamOne;
	}

	public synchronized final void setNameTeamTwo(String nameTeamTwo) {
		this.nameTeamTwo = nameTeamTwo;
	}

	public synchronized final void setScoreTeamOne(int scoreTeamOne) {
		this.scoreTeamOne = scoreTeamOne;
	}

	public synchronized final void setScoreTeamTwo(int scoreTeamTwo) {
		this.scoreTeamTwo = scoreTeamTwo;
	}

	public synchronized final void setNumberOfUpdates(int numberOfUpdates) {
		this.numberOfUpdates = numberOfUpdates;
	}

	public synchronized final void updatesPlus(){
		numberOfUpdates += 1;
	}

	public synchronized final void scoreOnePlus() {
		scoreTeamOne++;
	}

	public synchronized final void scoreTwoPlus() {
		scoreTeamTwo++;
	}

	public boolean isFirst() {
		return first;
	}

	public synchronized void setFirst(boolean first) {
		this.first = first;
	}
}
