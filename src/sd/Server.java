package sd;

import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.ArrayList;

final class Server {

	private Server() {
	}

	public static void main(String[] args) throws IOException {

		int port = 8082;

		System.out.print("Team name 1: ");

		BufferedReader brinput = new BufferedReader(new InputStreamReader(System.in));

		String nomeEquipa1 = brinput.readLine();

		System.out.println();

		System.out.print("Team name 2: ");

		String nomeEquipa2 = brinput.readLine();

		System.out.println();

		Game game = new Game(nomeEquipa1, nomeEquipa2);

		ServerSocket ss = null;

		ArrayList<Thread> threads = new ArrayList();

		try {

			ss = new ServerSocket(port);

			Thread http = new Http(game);

			http.start();

			while (true) {

				System.out.println("Waiting for client on port: " + port + "\n");

				Socket clientSocket = ss.accept();

				System.out.println("Recieved connection from client: " + clientSocket.getInetAddress() + " on port: " + clientSocket.getPort());

				Thread thread = new ServerSocketThread(clientSocket, game);

				thread.start();

				threads.add(thread);

				}

		} catch ( SocketException e ) {

			assert null != ss;
			ss.close();

		}

	}

}



