package sd;

final class Utils {

	private Utils() {
	}

	static int byteArray2Int(byte[] bytes) {

		int value = 0;

		for (int i = 0; i < bytes.length; i++) {

			value += (bytes[i] & 0xff) << (8 * i);

		}

		return value;

	}

}


