package sd;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerSocketThread extends Thread {

	private static final Logger LOGGER = Logger.getLogger(ServerSocketThread.class.getName());

	private final Socket clientSocket;

	private final Game game;

	private int contador;

	private boolean brk;

	private boolean first = true;

	ServerSocketThread(Socket clientSocket, Game game) {

		this.game = game;

		this.clientSocket = clientSocket;

		contador = 0;

	}

	@Override
	public final void run() {

		try {

			DataInputStream in = new DataInputStream(clientSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());

			while (!brk) {

				byte[] buffer = new byte[50];

				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				in.read(buffer);

				baos.write(buffer);

				while (0 < in.available()) {

					in.read(buffer);

					baos.write(buffer);

				}

				byte[] msg = baos.toByteArray();

				LOGGER.log(Level.FINE, msg.toString());

				contador = decodeClient2Server(msg);

				if (-99 == contador) {

					brk = true;

					System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nReceived exit message.");

					try {

						clientSocket.close();

					} catch (IOException e) {

						LOGGER.log(Level.SEVERE, e.toString(), e);

					}

					break;

				}

				game.printGameResults();

				byte[] jogoAtual = encodeServer2Client();

				out.write(jogoAtual);

				LOGGER.log(Level.FINE, jogoAtual.toString());

			}

			System.exit(0);

		} catch (Exception e) {

			LOGGER.log(Level.SEVERE, e.toString(), e);

		}

	}

	private byte[] encodeServer2Client() throws UnsupportedEncodingException {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		byte[] binNameOne = game.getNameTeamOne().getBytes("UTF-8");

		byte[] binNameTwo = game.getNameTeamTwo().getBytes("UTF-8");

		outputStream.write((byte) binNameOne.length);
		for (byte val : binNameOne) {
			outputStream.write(val);
		}

		outputStream.write((byte) binNameTwo.length);
		for (byte val : binNameTwo) {
			outputStream.write(val);
		}

		outputStream.write((byte) game.getScoreTeamOne());
		outputStream.write((byte) game.getScoreTeamTwo());

		outputStream.write((byte) game.getNumberOfUpdates());
		outputStream.write((byte) (game.getNumberOfUpdates() >>> 8));

		return outputStream.toByteArray();

	}

	private int decodeClient2Server(byte[] msg) {

		if ((-9 == (int) msg[0]) && (-9 == (int) msg[1])) {
			return -99;
		}

		byte[] nome = new byte[msg[2]];

		System.arraycopy(msg, 3, nome, 0, (int) msg[2]);

		byte[] count = new byte[2];
		count[0] = msg[(int) msg[2] + 3];
		count[1] = msg[(int) msg[2] + 4];

		boolean escreveu = false;
		if ((game.getNumberOfUpdates() == Utils.byteArray2Int(count)) && (0 != Utils.byteArray2Int(count))) {

			System.out.println("correu if");

			game.setScoreTeamOne((int) msg[0]);

			game.setScoreTeamTwo((int) msg[1]);

			game.updatesPlus();

			escreveu = true;

		}

		String nomeString;

		try {

			nomeString = new String(nome, "UTF-8");
			System.out.println("username: " + nomeString);
			if ( escreveu ) {
				game.getComments().add(new Comment("SERVER","<pre>game update by " + nomeString + " \nscore : "+game.getScoreTeamOne() + " - " + game.getScoreTeamTwo()+ "</pre>") );
			}

		} catch (UnsupportedEncodingException e) {

			System.out.println("String com caracteres invalidos");

		}



		return game.getNumberOfUpdates();

	}

}