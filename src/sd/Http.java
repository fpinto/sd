package sd;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Http extends Thread {

    private static Game game;

    private static final Logger LOGGER = Logger.getLogger(Http.class.getName());

    public Http(Game game) {

        Http.game = game;

        LOGGER.setLevel(Level.INFO);

    }

    static class Handler implements HttpHandler {

        Handler() {

        }

        public static String parsePostRequest(String post_request) throws UnsupportedEncodingException {

            String[] first = post_request.split("comment=");

            String[] second = first[1].split("&");

            String third = second[1];

            String[] fourth = third.split("username=");

            String comment = java.net.URLDecoder.decode(second[0].replace("+", ""), "UTF-8");

            String user = java.net.URLDecoder.decode(fourth[1].replace("+", ""), "UTF-8");

            game.getComments().add(new Comment(user, comment));

            return user;

        }

        public static String buildHtmlResponseGet(String t1, int g1, String t2, int g2) {

            int randomNum = ThreadLocalRandom.current().nextInt(100000, 1000000);

            return buildHtmlResponsePost(t1, g1, t2, g2, "anonymous_" + Integer.toString(randomNum));

        }

        public final void handle(HttpExchange exchange) {

            try {

                if ("POST".equalsIgnoreCase(exchange.getRequestMethod())) {

                    Headers requestHeaders = exchange.getRequestHeaders();

                    int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));

                    InputStream input = exchange.getRequestBody();

                    byte[] data = new byte[contentLength];

                    input.read(data);

                    System.out.println(new String(data));

                    String user = parsePostRequest(new String(data));

                    String response_string = buildHtmlResponsePost(game.getNameTeamOne(), game.getScoreTeamOne(), game.getNameTeamTwo(), game.getScoreTeamTwo(), user);

                    byte[] response_bytes = response_string.getBytes();

                    exchange.sendResponseHeaders(200, response_bytes.length);

                    OutputStream output = exchange.getResponseBody();

                    output.write(response_bytes);

                    output.close();


                } else if ("GET".equalsIgnoreCase(exchange.getRequestMethod())) {

                    String response_string = buildHtmlResponseGet(game.getNameTeamOne(), game.getScoreTeamOne(), game.getNameTeamTwo(), game.getScoreTeamTwo());

                    byte[] response_bytes = response_string.getBytes();

                    exchange.sendResponseHeaders(200, response_bytes.length);

                    OutputStream output = exchange.getResponseBody();

                    output.write(response_bytes);

                    output.close();

                }

            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();

                LOGGER.log(Level.WARNING, "Invalid character used in either comment or username");

            } catch (IOException e) {

                e.printStackTrace();

                LOGGER.log(Level.SEVERE, "Input Output exception");

            }

        }

    }

    private static String buildHtmlResponsePost(String t1, int g1, String t2, int g2, String user) {

        StringBuilder response = new StringBuilder("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\n" +
                "    <title>SD</title>\n" +
                "\n" +
                "    <meta name=\"viewport\" content=\"user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1\">\n" +
                "\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">\n" +
                "\n" +
                "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\"\n" +
                "          integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">\n" +
                "\n" +
                "    <style>\n" +
                "        .chatperson {\n" +
                "            display: block;\n" +
                "            border-bottom: 1px solid #eee;\n" +
                "            width: 100%;\n" +
                "            display: flex;\n" +
                "            align-items: center;\n" +
                "            white-space: nowrap;\n" +
                "            overflow: hidden;\n" +
                "            margin-bottom: 15px;\n" +
                "            padding: 4px;\n" +
                "        }\n" +
                "\n" +
                "        .chatperson:hover {\n" +
                "            text-decoration: none;\n" +
                "            border-bottom: 1px solid orange;\n" +
                "        }\n" +
                "\n" +
                "        .namechat {\n" +
                "            display: inline-block;\n" +
                "            vertical-align: middle;\n" +
                "        }\n" +
                "\n" +
                "        .chatperson .chatimg img {\n" +
                "            width: 40px;\n" +
                "            height: 40px;\n" +
                "            background-image: url('http://i.imgur.com/JqEuJ6t.png');\n" +
                "        }\n" +
                "\n" +
                "        .chatperson .pname {\n" +
                "            font-size: 18px;\n" +
                "            padding-left: 5px;\n" +
                "        }\n" +
                "\n" +
                "        .chatperson .lastmsg {\n" +
                "            font-size: 12px;\n" +
                "            padding-left: 5px;\n" +
                "            color: #ccc;\n" +
                "        }\n" +
                "        .tableBodyScroll tbody {\n" +
                "            display:block;\n" +
                "            max-height:60vh;\n" +
                "            overflow-y:scroll;\n" +
                "        }\n" +
                "        .tableBodyScroll thead, tbody tr {\n" +
                "            display:table;\n" +
                "            width:100%;\n" +
                "            table-layout:fixed;\n" +
                "        }\n" +
                "\n" +
                "    </style>\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n" +
                "\n" +
                "    <a class=\"navbar-brand\" href=\"#\">SD Web</a>\n" +
                "    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"\n" +
                "            aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n" +
                "        <span class=\"navbar-toggler-icon\"></span>\n" +
                "    </button>\n" +
                "\n" +
                "    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n" +
                "\n" +
                "        <ul class=\"navbar-nav mr-auto\">\n" +
                "            <li class=\"nav-item active\">\n" +
                "                <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n" +
                "            </li>\n" +
                "        </ul>\n" +
                "\n" +
                "    </div>\n" +
                "</nav>\n" +
                "<br>\n" +
                "<div class=\"container\">\n" +
                "    <div class=\"row\">\n" +
                "        <div class=\"col-4\">\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-6\">\n" +
                "                    <label>Equipa da casa</label>\n" +
                "                    <input class=\"form-control\" type=\"text\" value=\"" + t1 + "\" id=\"team1\" style=\"background: transparent;\"\n" +
                "                           readonly>\n" +
                "                </div>\n" +
                "                <div class=\"col-6\">\n" +
                "                    <label>Equipa visitante</label>\n" +
                "                    <input class=\"form-control\" type=\"text\" value=\"" + t2 + "\" id=\"team2\" style=\"background: transparent;\"\n" +
                "                           readonly>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <br>\n" +
                "            <div class=\"row\">\n" +
                "                <div class=\"col-12\">\n" +
                "                    <p style=\"font-size: 100px; text-align: center\"> " + g1 + " - " + g2 + " </p>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"col-8\">\n" +
                "            <div class=\"col-12\">\n" +
                "                <form action=\"http://127.0.0.1:8080/Etapa1SD/web/formulario.html\" method=\"POST\">\n" +
                "\n" +
                "                    <div class=\"form-group\">\n" +
                "\n" +
                "                        <div class=\"chatbody\">\n" +
                "                            <table class=\"table tableBodyScroll\" style=\"\n" +
                "    height: 50vh;\" >\n");

        for (int i = 0; i < game.getComments().size(); i++) {

            response.append("                                <tr>\n" + "                                    <td style=\"width: 200px\">").append(game.getComments().get(i).getUser()).append("</td>\n").append("                                    <td> ").append(game.getComments().get(i).getComment()).append(" </td>\n").append("                                </tr>\n");
        }

        response.append("\n" + "                            </table>\n" + "                        </div>\n" + "\n" + "                    <textarea class=\"form-control\" id=\"comment\" name=\"comment\" rows=\"3\"></textarea>\n" + "                    </div>\n" + "\n" + "                    <div class=\"form-group\">\n" + "                        <label for=\"username\">Username</label>\n" + "                        <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" value=\"").append(user).append("\">\n").append("                    </div>\n").append("\n").append("                    <button type=\"submit\" class=\"btn btn-secondary\">Submit</button>\n").append("\n").append("\n").append("                </form>\n").append("\n").append("            </div>\n").append("        </div>\n").append("    </div>\n").append("</div>\n").append("\n").append("<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\"\n").append("        integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\"\n").append("        crossorigin=\"anonymous\"></script>\n").append("\n").append("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\"\n").append("        integrity=\"sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh\"\n").append("        crossorigin=\"anonymous\"></script>\n").append("\n").append("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js\"\n").append("        integrity=\"sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ\"\n").append("        crossorigin=\"anonymous\"></script>\n").append("\n").append("</body>\n").append("</html>");

        return response.toString();

    }

    @Override
    public final void run() {

        HttpServer server = null;

        try {

            server = HttpServer.create(new InetSocketAddress(8080), 0);

        } catch (IOException e) {

            e.printStackTrace();

            LOGGER.log(Level.SEVERE, "Check if port is unused");

        }

        assert server != null;

        server.createContext("/SDWeb.html", new Http.Handler());

        server.setExecutor(null);

        server.start();

    }

}
